from CountryPolicies import *
import FloatCheck
class Product :
    def __init__(self,product_id = None,name = None,price = None,country = None,sales_tax = None,total_price = None):
        self.product_id = product_id
        self.name = name
        self.price = price
        self.country = country
        self.sales_tax = sales_tax
        self.total_price = total_price

    def generate_output(self,product_obj):
        float_check_obj = FloatCheck.FloatCheck()
        india_obj = India.India()
        egypt_obj = Egypt.Egypt()
        japan_obj = Japan.Japan()
        price = product_obj.price
        tax_multiplier = 0
        if float_check_obj.float_check(price):
            price = float(price)
            if product_obj.country == "india":
                tax_multiplier = india_obj.tax_multiplier_calculator(price)
            elif product_obj.country == "egypt":
                tax_multiplier = egypt_obj.tax_multiplier_calculator(price)
            elif product_obj.country == "japan":
                tax_multiplier = japan_obj.tax_multiplier_calculator(price)
            sales_tax = price * tax_multiplier
            total_price = price + sales_tax
            product_obj.sales_tax = sales_tax
            product_obj.total_price = total_price
        else:
            product_obj.sales_tax = "n/a"
            product_obj.total_price = "n/a"
        return product_obj