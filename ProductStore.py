from google.appengine.ext import db

class ProductStore(db.Model):
	product_id = db.StringProperty()
	name = db.StringProperty()
	price = db.FloatProperty()
	country = db.StringProperty(choices=set(["india", "japan", "egypt"]))
	sales_tax = db.FloatProperty()
	total_price = db.FloatProperty()