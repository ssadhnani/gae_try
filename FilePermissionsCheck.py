import os
import sys
class FilePermissionsCheck :
	def readability_check(self,file_to_check):
		return os.access(file_to_check,os.R_OK)
	def writeability_check(self,file_to_check):
		return os.access(file_to_check,os.W_OK)