import jinja2
import os
import webapp2
import Product
import ProductStore

template_env = jinja2.Environment(loader=jinja2.FileSystemLoader(os.getcwd()))

class SingleOutput(webapp2.RequestHandler):
	def get(self):
		template = template_env.get_template('index.html')
		self.response.write(template.render())


	def post(self):
		product_obj = Product.Product()
		product_id = self.request.get("id")
		name = self.request.get("name")
		price = self.request.get("price")
		country = self.request.get("country")
		product_obj.product_id = product_id
		product_obj.name = name
		product_obj.price = price
		product_obj.country = country
		product_obj = product_obj.generate_output(product_obj)
		sales_tax = product_obj.sales_tax
		total_price = product_obj.total_price
		price = float(price)
		context = {'product_id': product_id, 'name': name, 'price': price, 'country': country, 'sales_tax': sales_tax, 'total_price': total_price }
		template = template_env.get_template('index1.html')
		product_store_obj = ProductStore.ProductStore( product_id = product_id, name = name, price = price, country = country, sales_tax = sales_tax, total_price = total_price )
		product_store_key = product_store_obj.put()
		self.response.write(template.render(context))
		

class List(webapp2.RequestHandler):
	def get(self):
		product_store_obj = ProductStore.ProductStore().all().fetch(100)
		key = {'data':product_store_obj}
		template = template_env.get_template('list.html')
		self.response.write(template.render(key))

app = webapp2.WSGIApplication([('/', SingleOutput),('/list',List)])


#application = webapp2.WSGIApplication([('/', MainPage)], debug=True)
