class India :
	def tax_multiplier_calculator(self,price):
		tax_multiplier = 0
		if price > 100 and price < 500 :
			tax_multiplier = 0.1
		elif price > 500 and price < 1000:
			tax_multiplier = 0.15
		elif price > 1000 :
			tax_multiplier = 0.2
		return tax_multiplier
